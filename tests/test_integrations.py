# coding: utf8
import pytest
import os
from app import app
from flask import url_for

@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'test'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client

def test_base(client):
    rv =  client.get('/')
    assert rv.status_code == 200
    assert b'The world of Jurassic Park' in rv.data
    assert b'Discover the Dilophosaurus' in rv.data
    assert b'Discover the Brachiosaurus' in rv.data
    assert b'Discover the Velociraptor' in rv.data
    assert b'Discover the Parasaurolophus' in rv.data
    assert b'Discover the Triceratops' in rv.data
    assert b'Discover the Tyrannosaurus rex' in rv.data
    assert b'Discover the Gallimimus' in rv.data

def test_desc_dino(client):
    rv = client.get(url_for("desc_dino",slug = "dilophosaurus"))
    assert rv.status_code == 200

    rv = client.get(url_for("desc_dino",slug = "brachiosaurus"))
    assert rv.status_code == 200

    rv = client.get(url_for("desc_dino",slug = "velociraptor"))
    assert rv.status_code == 200

    rv = client.get(url_for("desc_dino",slug = "parasaurolophus"))
    assert rv.status_code == 200

    rv = client.get(url_for("desc_dino",slug = "triceratops"))
    assert rv.status_code == 200

    rv = client.get(url_for("desc_dino",slug = "tyrannosaurus-rex"))
    assert rv.status_code == 200

    rv = client.get(url_for("desc_dino",slug = "gallimimus"))
    assert rv.status_code == 200