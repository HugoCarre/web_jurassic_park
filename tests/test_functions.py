# coding: utf8
import pytest
from libraries.dinoManager import getDino, getListDino, getTopRated

def test_getDino():
    listDino = getListDino()
    assert -1 !=  listDino                  # -1 valeur d'erreur
    assert 7 ==  len(listDino)              # 7 dinosaures dans la liste

def test_getListDino():
    listDino = getListDino()
    for dino in listDino:
        assert -1 != getDino(dino["slug"])        #-1 valeur erreur des fonctions
        assert 10 == len(getDino(dino["slug"]))   #Un dino contient 10 éléments 

def test_getTopRated():
    assert -1 != getTopRated()             # -1 valeur d'erreur
    assert 3 == len(getTopRated())         # top rated affiche trois dinosaures
