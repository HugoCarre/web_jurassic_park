# coding: utf8
from flask import Flask
from flask import render_template
from libraries.dinoManager import getTopRated,getListDino,getDino
from flaskext.markdown import Markdown

app = Flask(__name__)
Markdown(app)

@app.route("/")
def base():
    return render_template("list_dino.html", list=getListDino())

@app.route("/dino/<slug>")
def desc_dino(slug=None):
    return render_template("dino.html",dino=getDino(slug),topRated=getTopRated())