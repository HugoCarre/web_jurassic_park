# coding: utf8
import requests
import random

def getListDino():
    """
    Get the list of dinosaurs
    :return: dinosaurs list
    :rtype: dict
    """
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    if response.status_code == 200 :
        listDino = response.json()
    else:
        return -1
    return listDino

def getDino(slug):
    """
    Get the dinosaur corresponding to the slug
    :param slug: slug to complete url to get dinosaur
    :return: the dinosaur
    :rtype: dict
    """
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/'+ slug)
    if response.status_code == 200:
        dino = response.json()
    else:
        return -1
    return dino    

def getTopRated():
    """
    Get three random dinosaurs in the list
    :return: the three random dinosaurs
    :rtype: list
    """
    listDino = getListDino()
    if listDino == -1:
        return -1
    else: 
        return random.sample(listDino,3)
